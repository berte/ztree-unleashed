#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

source read_settings.sh

echo
echo "$(tput setaf 3)Which resolution should the virtual screens have?"
echo "$(tput setaf 3)Please enter it in the format widthxheight, for"
echo "$(tput setaf 3)example 1280x760 or 800x600. You should always"
echo "$(tput setaf 3)use the smallest plausible value!"
echo "$(tput setaf 3)This is usually implied by your existing lab"
echo "$(tput setaf 3)hardware. If you don't know, enter 1024x768.$(tput sgr0)"
echo

while :
do
	echo "$(tput setaf 5)Please enter the desired screen resolution or press"
	echo "$(tput setaf 5)enter to keep the current value (${GEOMETRY}):$(tput sgr0)"

	read -r entry


	if [[ ${entry} =~ [0-9]x[0-9] || ${entry} == "" ]]
	then
		break
	else
		echo "$(tput setaf 1)Invalid input. Enter the screen size"
		echo "$(tput setaf 1)in the format widthxheight."
		echo "$(tput setaf 1)Width and height must be numeric.$(tput sgr0)"

	fi
done

if [[ ${entry} == "" ]]
then
	entry=${GEOMETRY}
fi

echo "${entry}" > /share/geometry
echo "Set screen resolution to ${entry}."
echo

# ----------------------------------------------------------

echo
echo "$(tput setaf 3)Which font size should be used in z-Leaf?"
echo

echo "$(tput setaf 5)Please enter the desired font size or press"
echo "$(tput setaf 5)enter to keep the current value (${FONTSIZE}):$(tput sgr0)"

read -r entry

if [[ ${entry} == "" ]]
then
	entry=${FONTSIZE}
fi


echo "${entry}" > ~/.zu/fontsize
echo "Set font size to ${entry}."
echo

# ----------------------------------------------------------

if [ ! -f ~/.zu/prefix ]; then
	echo "${PREFIX}" > ~/.zu/prefix
fi

# ----------------------------------------------------------

echo
echo "$(tput setaf 3)Please enter the language of z-Tree and z-Leaf."
echo "$(tput setaf 3)Only enter one of the \"Command Line Option\"s listed"
echo "$(tput setaf 3)in the manual on page 171, e.g. de, suomi or english."
echo

echo "$(tput setaf 5)Press enter to keep the current value (${ZTREELANG}).$(tput sgr0)"

read -r entry

if [[ ${entry} == "" ]]
then
        entry=${ZTREELANG}
fi

echo "${entry}" > ~/.zu/language
echo "Set language to ${entry}."
echo

# ----------------------------------------------------------

echo
echo "$(tput setaf 3)Please enter how many users should be started by default."
echo "$(tput setaf 3)(This does not affect the number of clients on this system,"
echo "$(tput setaf 3)which is fixed. It's merely the default for when you"
echo "$(tput setaf 3)start a session. You can change it at any time.)"
echo "$(tput setaf 3)If you are unsure, we recommend entering \"32\"."
echo

while :
do
	echo "$(tput setaf 5)How many users should be started by default? [1-${NCLIENTS}]"
	echo "$(tput setaf 5)Press enter to keep the current value (${NCLIENTS_START}).$(tput sgr0)"

	read -r entry

	if [[ (${entry} -lt 1 || ${entry} -gt ${NCLIENTS}) && ${entry} != "" ]]
	then
		echo "$(tput setaf 1)Invalid input. Enter a natural number between 1 and ${NCLIENTS}.$(tput sgr0)"
	else
		break
	fi
done

if [[ ${entry} == "" ]]
then
	entry=${NCLIENTS_START}
fi

echo "${entry}" > ~/.zu/nclients_start
echo "Set default number of clients to ${entry}."
echo

# ----------------------------------------------------------

echo
echo "$(tput setaf 3)Which model do you want to use?"
echo
echo "$(tput setaf 3) 0: This device is both the z-Tree unleashed server"
echo "$(tput setaf 3)    and the reverse proxy. This device is (globally)"
echo "$(tput setaf 3)    reachable through its IP address, and subjects"
echo "$(tput setaf 3)    will directly access this server. Perhaps you are"
echo "$(tput setaf 3)    setting up a server in a datacenter."
echo
echo "$(tput setaf 3) 1: This device is only the z-Tree unleashed server."
echo "$(tput setaf 3)    You use an additional \"Thin\" server which subjects"
echo "$(tput setaf 3)    will use to indirectly connect with this device."
echo "$(tput setaf 3)    Perhaps you are behind a firewall or NAT and you"
echo "$(tput setaf 3)    may consider yourself tremendously mobile."
echo
echo "$(tput setaf 1)$(tput bold)If you don't know, enter 0.$(tput sgr0)$(tput setaf 3) If you enter 1, a WireGuard"
echo "$(tput setaf 3)config will be automatically generated in the next step"
echo "$(tput setaf 3)if no config has yet been created."
echo

echo "$(tput setaf 5)Please enter the desired model (0 or 1) or press"
echo "$(tput setaf 5)enter to keep the current value (${USE_THIN}):$(tput sgr0)"

read -r entry

if [[ ${entry} == "" ]]
then
	entry=${USE_THIN}
fi

if [[ ${entry} == "1" ]]
then
	echo "Selected model 1."
	echo 1 > ~/.zu/use_thin
	
	if [ -f /share/wg_my_ip ]; then
		echo "$(tput setaf 1)Since a WireGuard already exists, no"
		echo "$(tput setaf 1)additional config will be generated."
		echo "$(tput setaf 1)Delete /share/wg_my_ip and re-run this"
		echo "$(tput setaf 1)program if this is not expected."
		
		cat /share/wg_my_ip > ~/.zu/listen
		cat /share/wg_my_ip > ~/.zu/zuserver
	else
		echo "$(tput setaf 1)You will now be able to create a WireGuard config."
		echo
		cd wg || exit 1
		./generate_wg.sh
		cd ..
		echo
		if [ -f /share/wg_my_ip ]; then
			echo "$(tput setaf 3)Welcome back. Set your IP to $(cat /share/wg_my_ip)."
			echo "$(tput setaf 3)If you are running me under the graphical interface,"
			echo "$(tput setaf 3)I will now open the \"Thin\" server's config. But"
			echo "$(tput setaf 3)you can also find it under /home/lab/scripts/wg/Thin_server/wg_zu.conf"
			
			geany /home/lab/scripts/wg/Thin_server/wg_zu.conf >/dev/null 2>&1 &
			
			cat /share/wg_my_ip > ~/.zu/listen
			cat /share/wg_my_ip > ~/.zu/zuserver
		else
			echo "$(tput setaf 1)Welcome back. Apparently something didn't work out."
			echo "$(tput setaf 3)I am choosing model 0 for you. Please re-run this."
			echo "$(tput setaf 3)program if necessary."
			
			echo "127.0.0.1" > ~/.zu/listen
			echo "127.0.0.1" > ~/.zu/zuserver
			echo 0 > ~/.zu/use_thin
		fi
		
	fi
else
	echo "Selected model 0."
	
	echo "127.0.0.1" > ~/.zu/listen
	echo "127.0.0.1" > ~/.zu/zuserver
	echo 0 > ~/.zu/use_thin
fi

USE_THIN=$(cat ~/.zu/use_thin)

echo

# ----------------------------------------------------------

echo
echo "$(tput setaf 3)What is the host name (\"domain name\") of the reverse"
echo "$(tput setaf 3)proxy (i.e. this server or the \"Thin\" server)?"
echo "$(tput setaf 3)If you are merely testing z-Tree unleashed, you can"
echo "$(tput setaf 3)keep 127.0.0.1 -- but if you truly wish to invite"
echo "$(tput setaf 3)subjects, this will be in the link they are sent."
echo "$(tput setaf 3)In other words, the reverse proxy must be publicly"
echo "$(tput setaf 3)reachable under this host name."
echo

echo "$(tput setaf 5)Please enter the desired host name or press"
echo "$(tput setaf 5)enter to keep the current value (${HOST}):$(tput sgr0)"

read -r entry

if [[ ${entry} == "" ]]
then
	entry=${HOST}
fi

echo "${entry}" > ~/.zu/host
echo "Set host name to ${entry}."
echo

# ----------------------------------------------------------

if [[ ${USE_THIN} == "0" ]]
then
	echo
	echo "$(tput setaf 3)Finally, two quick questions for nerds. Please"
	echo "$(tput setaf 3)specify the paths of the SSL certificate and private"
	echo "$(tput setaf 3)key on the reverse proxy. The reverse proxy must be"
	echo "$(tput setaf 3)able to find these files -- they are on this device."
	echo
	echo "$(tput setaf 3)If you don't know and you are merely testing, feel"
	echo "$(tput setaf 3)free to keep the defaults (a self-signed certificate),"
	echo "$(tput setaf 3)but be aware that this creates an ugly warning in your"
	echo "$(tput setaf 3)browser. For production use, consider procuring an"
	echo "$(tput setaf 3)official X.509 certificate."
	echo

	echo "$(tput setaf 5)Please enter the path of the SSL certificate or press"
	echo "$(tput setaf 5)enter to keep the current value (${SSL_CERT}):$(tput sgr0)"

	read -r entry

	if [[ ${entry} == "" ]]
	then
		entry=${SSL_CERT}
	fi

	echo "${entry}" > ~/.zu/ssl_cert
	echo "Set path of the SSL certificate to ${entry}."
	echo

	echo "$(tput setaf 5)Please enter the path of the SSL private key or press"
	echo "$(tput setaf 5)enter to keep the current value (${SSL_KEY}):$(tput sgr0)"

	read -r entry

	if [[ ${entry} == "" ]]
	then
		entry=${SSL_KEY}
	fi

	echo "${entry}" > ~/.zu/ssl_key
	echo "Set path of the SSL private key to ${entry}."
	echo
fi
