#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

NFILES=$(find /share -type f -iname '*.exe' | grep -Fv vorschaltung | grep -Fvc preleaf)

if [ "${NFILES}" -gt 0 ]; then
	echo "$(tput setaf 3)The following files were found:$(tput sgr0)"
	echo
	find /share -type f -iname '*.exe' | grep -Fv vorschaltung | grep -Fv preleaf | cat -n
	echo
	while :
	do
		echo "$(tput setaf 5)Please enter your selection [1-${NFILES}]:$(tput sgr0)"
		read -r selection

		if [[ "${selection}" -lt 1 || "${selection}" -gt "${NFILES}" ]]
		then
			echo "$(tput setaf 1)Invalid input. Enter a natural number between 1 and ${NFILES}.$(tput sgr0)"
		else
			break
		fi
	done

	find /share -type f -iname '*.exe' | grep -Fv vorschaltung | grep -Fv preleaf | head -n "${selection}" | tail -n 1 >&2
else
	echo "$(tput setaf 1)No files were found. Please move z-Leaf and z-Tree into"
	echo "$(tput setaf 1)/share/zTree. They will then be found automatically. Stopping now."
	exit 1
fi
