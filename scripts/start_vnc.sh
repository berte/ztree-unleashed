#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

# usage: ./start_vnc.sh user display port pw

./as_another_tmux.sh "$1" "x11vnc -listen localhost -rfbport $3 -display :$2 -shared -forever -loop -passwd $4 -viewpasswd $(cat ~/.zu/viewpasswd)"
