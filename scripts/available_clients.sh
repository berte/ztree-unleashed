#!/usr/bin/env bash

source read_settings.sh

{
    for i in $(seq 1 "${NCLIENTS}")
    do
        echo -n "${PREFIX}${i} "
    done
    echo
} | fold -s -w 60
