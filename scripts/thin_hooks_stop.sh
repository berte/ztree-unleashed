#!/usr/bin/env bash

# in this script, you can put commands or scripts
# that are executed after a session has been terminated

# to use the FreeBSD example, uncomment this line:
# source ~/ztree-unleashed/extras/example_freebsd/hook_stop.sh
