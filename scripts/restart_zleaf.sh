#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

set -euo pipefail

source read_settings.sh

echo "The following clients are available:$(tput sgr0)"
echo

./available_clients.sh

echo
echo "$(tput setaf 6)Which z-Leaf do you want to restart?"
echo "$(tput setaf 1)NOTE: You might lose data if you proceed."
tput sgr0
read -r user

sudo pkill --signal=INT -u "${user}" wine
sleep 0.4
VDISP=$(grep -wE "${user}" /share/.session/sessionfile | awk '{print $2}')

if [[ -n "${VDISP}" ]]
then
    ./start_zleaf.sh "${user}" "${VDISP}" "$(sudo cat "/home/${user}/.zu/core")" "${FONTSIZE}" "$(cat /share/.session/channel)"
    
    echo "Restarted z-Leaf on ${user}."
fi
