#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.1. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

if sudo pgrep Xvfb > /dev/null; then
	tput setaf 1
	echo "It seems like you are currently running a session. I refuse to"
	echo "shut down. Please \"Terminate session\" first."

	exit 1
fi

sudo poweroff
