#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

source read_settings.sh

awk '{print "https://'"${HOST}"'/"$5"/?p="$6}' /share/.session/sessionfile

# note that $5 is the port of the reverse proxy
# $4 would be the noVNC port (but then you have to replace https by http)
# don't do that! it's not secure, except over the loopback device (for tests)
