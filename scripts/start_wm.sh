#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

# usage: ./start_wm.sh user display

if [ -x "$(command -v i3)" ]; then
	sudo mkdir -p "/home/$1/.config/i3"
	sudo chown "$1" "/home/$1/.config/i3"
	echo "new_window 1pixel" | sudo tee "/home/$1/.config/i3/config" > /dev/null

	./as_another_tmux.sh "$1" "bash -c 'sleep 2; DISPLAY=:$2 i3'"
	./as_another_tmux.sh "$1" "bash -c 'sleep 5; pkill i3bar'"
fi
