#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

source read_settings.sh

VNCPORT=$(grep -wE "${PREFIX}0" /share/.session/sessionfile | awk '{print $3}')

grep -wE "${PREFIX}0" /share/.session/sessionfile | awk '{print $6}' | vncviewer -autopass "127.0.0.1::${VNCPORT}"
