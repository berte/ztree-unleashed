#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

SEG1=$((RANDOM % 256))
SEG2=$((RANDOM % 256))
PORT=$((49152 + RANDOM % 16384))
SKP1=$(wg genkey)
SKP2=$(wg genkey)
PKP1=$(echo "${SKP1}" | wg pubkey)
PKP2=$(echo "${SKP2}" | wg pubkey)

echo "$(tput setaf 2)z-Tree unleashed"
echo "$(tput setaf 2)WireGuard config generator"

echo "$(tput setaf 3)Please enter the IP of the \"Thin\" server."
echo "$(tput setaf 3)You can directly enter IPv4 addresses, but"
echo "$(tput setaf 3)please enclose IPv6 addresses in [ and ]."
echo "$(tput setaf 3)You may also enter a hostname."
echo "$(tput setaf 4)Example (IPv4): 192.168.2.55"
echo "$(tput setaf 4)Example (IPv6): [fd42::2:55]"
echo "$(tput setaf 4)Example (Hostname): example.com$(tput sgr0)"
echo
echo "$(tput setaf 5)Enter the IP of the \"Thin\" server:$(tput sgr0)"
read -r IPP1

echo

rm -rf Thin_server zu_server
mkdir Thin_server zu_server

cp .Thin.conf.dist Thin_server/wg_zu.conf
cp .zu.conf.dist zu_server/wg_zu.conf

sed -i "s!SEG1!${SEG1}!g" Thin_server/wg_zu.conf
sed -i "s!SEG2!${SEG2}!g" Thin_server/wg_zu.conf
sed -i "s!PORT!${PORT}!g" Thin_server/wg_zu.conf
sed -i "s!SKP1!${SKP1}!g" Thin_server/wg_zu.conf
sed -i "s!PKP2!${PKP2}!g" Thin_server/wg_zu.conf

sed -i "s!SEG1!${SEG1}!g" zu_server/wg_zu.conf
sed -i "s!SEG2!${SEG2}!g" zu_server/wg_zu.conf
sed -i "s!PORT!${PORT}!g" zu_server/wg_zu.conf
sed -i "s!SKP2!${SKP2}!g" zu_server/wg_zu.conf
sed -i "s!PKP1!${PKP1}!g" zu_server/wg_zu.conf
sed -i "s!IPP1!${IPP1}!g" zu_server/wg_zu.conf

echo "$(tput setaf 3)The following configs have been generated:"
echo "$(tput setaf 3)For the \"Thin\" server: $(tput setaf 4)$(pwd)/Thin_server/wg_zu.conf"
echo "$(tput setaf 3)For this server/VM: $(tput setaf 4)$(pwd)/zu_server/wg_zu.conf"
echo
echo "$(tput setaf 3)The \"Thin\" server is 10.${SEG1}.${SEG2}.1, and we are 10.${SEG1}.${SEG2}.2."
echo "10.${SEG1}.${SEG2}.1" > /share/wg_thin_ip
echo "10.${SEG1}.${SEG2}.2" > /share/wg_my_ip
echo
echo "$(tput setaf 5)Do you wish to install the local config?"

while :
do
    echo "$(tput setaf 5)Enter yes or no.$(tput sgr0)"
    read -r decision

    if [[ "${decision}" == "yes" ]]
    then
        sudo cp zu_server/wg_zu.conf /etc/wireguard/
        sleep 1
        sudo systemctl start wg-quick@wg_zu.service
        sudo systemctl enable wg-quick@wg_zu.service

        echo 1 > ~/.zu/use_thin

        echo "$(tput setaf 3)Local config installed, started and enabled."
        echo "$(tput setaf 3)As soon as you have installed the config on"
        echo "$(tput setaf 3)the \"Thin\" server, you can check whether"
        echo "$(tput setaf 3)everything worked by executing"
        echo "$(tput setaf 4)ping -c4 10.${SEG1}.${SEG2}.1"
        echo "$(tput setaf 5)Good luck and goodbye.$(tput sgr0)"
        break
    else
        if [[ "${decision}" == 'no' ]]
        then
            echo "$(tput setaf 3)OK, you're on your own. Good luck!"
            break
        fi
    fi
done
