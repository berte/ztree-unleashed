#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

if [ ! -f ~/ztree-unleashed/extras/enable_rtf/riched20.dll ]; then
	echo "ERROR: riched20.dll not found. Stopping."
	echo "Before running this program, you need to copy"
	echo "riched20.dll from C:\\Windows\\SysWOW64 into"
	echo "/home/lab/ztree-unleashed/extras/enable_rtf"
	exit 1
fi

set -euo pipefail

cp ~/ztree-unleashed/extras/enable_rtf/riched20.dll /tmp

./list_clients.sh | while read -r line;
do
	sudo mkdir -p "/home/${line}/.wine/drive_c/windows/system32"
	sudo cp /tmp/riched20.dll "/home/${line}/.wine/drive_c/windows/system32"
	sudo chown "${line}:users" "/home/${line}/.wine/drive_c/windows/system32/riched20.dll"
done

touch ~/.zu/rtf_enabled

echo "Done."
