#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

./list_clients.sh | while read -r line ; do sudo userdel -r "${line}" ; done

sudo rm -f /etc/zu_oem_users
