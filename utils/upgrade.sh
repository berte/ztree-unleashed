#!/usr/bin/env bash

set -euo pipefail

git clone --depth 1 https://github.com/novnc/noVNC
git clone --depth 1 https://github.com/novnc/websockify noVNC/utils/websockify

cd noVNC
git log --format="%H" -n 1 > ../.versions

cd utils/websockify
git log --format="%H" -n 1 >> ../../../.versions

cd ../../..

rm -rf noVNC/.git
rm -rf noVNC/utils/websockify/.git

ln -s ../custom_portal.html noVNC/index.html
find . -name '.gitignore' -delete
